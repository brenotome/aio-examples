from datetime import datetime
from time import time

def print_2s_and_3s():
    start = time()
    twos = 0 
    threes = 0 
    
    while True:
        if ((time()-start) // 2 == twos):
            print(f"two seconds passed it's {datetime.now()}")
            twos += 1
        if ((time()-start) // 3 == threes):
            print(f"three seconds passed it's {datetime.now()}")        
            threes += 1

if __name__ == '__main__':
    print_2s_and_3s()