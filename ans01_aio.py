from datetime import datetime
import asyncio

async def print_at_2s():
    while True:
        print(f"two seconds passed it's {datetime.now()}")
        await asyncio.sleep(2)


async def print_at_3s():
    while True:
        print(f"three seconds passed it's {datetime.now()}")
        await asyncio.sleep(3)


async def main():
    await asyncio.gather(
        print_at_2s(),
        print_at_3s()
        )    
    
if __name__ == '__main__':
    loop = asyncio.run(main())