from threading import Thread
from datetime import datetime
from time import sleep

def print_at_2s():
    print('hello2')
    while True:
        print(f"two seconds passed it's {datetime.now()}")
        sleep(2)


def print_at_3s():
    print('hello3')
    while True:
        print(f"three seconds passed it's {datetime.now()}")
        sleep(3)
    

if __name__ == '__main__':
    Thread(target = print_at_2s).start()
    Thread(target = print_at_3s).start()
    