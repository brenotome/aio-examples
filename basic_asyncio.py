import time
import asyncio
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor

async def async_function():
    print('hi')
    return 1

async def async_echo_name(name: str):
    print(f'hi {name}')    
    return 2

def slow():
    time.sleep(10)
    print('hi hubinho')

async def run_blocker(function):
    await asyncio.to_thread(function)
    return 3

async def run_blocker_retro(function):
    '''python < 3.9'''
    loop = asyncio.get_event_loop()
    await loop.run_in_executor(ThreadPoolExecutor(), slow)
    await loop.run_in_executor(ProcessPoolExecutor(), slow)


async def run_concurrent(*functions):
    response = await asyncio.gather(*functions)
    pass

if __name__ == "__main__":
    asyncio.run(
        run_concurrent(
            run_blocker(slow),
            run_blocker_retro(slow),
            async_function(), 
            async_echo_name('Boberson'),
        )
    )
    
