import uvicorn
import json
import time 
import httpx
import requests
import asyncio

# handlers
async def handle_post(scope, body):
    return {'response': 'asdf'}


def handle_post_sync(scope, body):
    return {'response': 'asdf'}


async def handle_get(scope, body:dict):
    async with httpx.AsyncClient() as client:
        response = await client.get(f'https://pokeapi.co/api/v2/pokemon-species/{ body.get("message", 1) }')
        return response.json()
    
async def handle_get_10_async(scope, body:dict):    
    tasks = [handle_get(scope, {"message":i}) for i in range(1,10)]
    _ = await asyncio.gather(asyncio.wait(tasks))
    return {"message": "ran 10 querys"}

def handle_get_10_sync(scope, body:dict):    
    for i in range(1,10):
        _ = handle_get_sync(scope, {"message":i})
    return {"message": "ran 10 querys"}
    

def handle_get_sync(scope, body):
    response = requests.get(f'https://pokeapi.co/api/v2/pokemon-species/{ body.get("message", 1) }')
    return response.json()


async def handle_stream(scope, body):
    for i in body['message']:
        yield {'response': str(i).upper()}


# app asgi
async def app(scope, receive, send):
    headers = [(b'content-type', b'text/html')]
    
    body = b''
    more_body = True
    while more_body:
        message = await receive()
        body += message.get("body", b"")
        more_body = message.get("more_body", False)
    
    parsed_body = json.loads(body)
    assert 'message' in parsed_body

    response = ''
    match scope:
        case {'method': 'POST', 'path':'/async', **rest}:
            response = await handle_post(scope, parsed_body)
        case {'method': 'POST', 'path':'/sync', **rest}:
            response = handle_post_sync(scope, parsed_body)
        case {'method': 'GET', 'path':'/async', **rest}:
            response = await handle_get(scope, parsed_body)
        case {'method': 'GET', 'path':'/sync', **rest}:
            response = handle_get_sync(scope, parsed_body)
        case {'method': 'GET', 'path':'/10/sync', **rest}:
            response = handle_get_10_sync(scope, parsed_body)
        case {'method': 'GET', 'path':'/10/async', **rest}:
            response = await handle_get_10_async(scope, parsed_body)
        case {'method': 'POST', 'path':'/stream', **rest}:            
            await send({"type": "http.response.start", "status":200, "headers": headers})
            async for response in handle_stream(scope, parsed_body):
                await send({"type": "http.response.body", "body":str(response).encode(), "more_body": True})
            await send({"type": "http.response.body", "more_body": False})
            return

    
    await send({"type": "http.response.start", "status":200, "headers": headers})
    await send({"type": "http.response.body", "body":str(response).encode()})


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000, workers=10)
