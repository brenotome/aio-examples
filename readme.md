### Requests para testar endpoints de ex02

```Bash
# executa rota com request síncrono à pokeapi
curl -X GET -H "Content-Type: application/json" -d '{"message":1}' http://localhost:8000/sync
# executa rota com request assíncrono à pokeapi
curl -X GET -H "Content-Type: application/json" -d '{"message":1}' http://localhost:8000/async
# executa rota com resposta estática síncrona
curl -X POST -H "Content-Type: application/json" -d '{"message":1}' http://localhost:8000/sync
# executa rota com resposta estática assíncrona
curl -X POST -H "Content-Type: application/json" -d '{"message":1}' http://localhost:8000/async

# executa rota com 10 requests síncronos à pokeapi
curl -X GET -H "Content-Type: application/json" -d '{"message":1}' http://localhost:8000/10/sync
# executa rota com 10 requests assíncronos à pokeapi
curl -X GET -H "Content-Type: application/json" -d '{"message":1}' http://localhost:8000/10/async

```

## benchmarking

Para fazer o benchmarking pode se usar a ferramenta wrk
para instalar em ambiente debian
```
sudo apt install wrk
```

Para executar o benchmarking de cada rota altere o arquivo .lua correspondente 

No comando abaixo substitua de acordo com seu hardware ou rota que quer testar
- -t : número de threads enviando requests
- -c : número de conexões TCP ativas
- -d : por quanto tempo continuar o benchmarking
- arquivo.lua : contém dados do request a ser enviado
```
wrk -t10 -c 10 -d30s -s post.lua  http://localhost:8000/async
```